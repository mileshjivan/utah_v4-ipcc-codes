/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.IPCC_Codes_V5_2_PageObjects;
/**
 *
 * @author SKhumalo
 */
public class IPCC_Codes_PageObject {

    public static String Activity() {
        return "(//div[@id='control_A5598BDE-BB73-41F5-AE3B-C120B0E841F8']//input)[1]";
    }
    public static String Activity2() {
        return "(//div[@id='control_5AD0CBAE-05AF-4994-B86A-34E4E3B162A7']//input)[1]";
    }
    public static String Activity3() {
        return "(//div[@id='control_8A261C31-751A-44B6-A3A2-10656982F6C9']//input)[1]";
    }
    public static String Activity4() {
        return "(//div[@id='control_27E8B44A-83AF-420D-86C6-DC3898E1D3E5']//input)[1]";
    }
    public static String ContactNumber() {
        return "(//div[@id='control_79F95CF9-F0EB-447A-8878-B91B111163B3']//input)[1]";
    }
    public static String IPCC_Codes_Header() {
        return "//div[text()='IPCC Codes']";
    }
    public static String EmailAddress() {
        return "(//div[@id='control_B6F2BD1D-781B-4FB3-B6B9-E9C00DBB00E9']//input)[1]";
    }
    public static String Emergency_Response_Teams() {
        return "//label[text()='Emergency Response Teams']";
    }
    public static String IPCC_Codes() {
        return "//label[text()='IPCC Codes']";
    }
    public static String IPCC_Codes_Level_2_Add_Button() {
        return "//div[@id='control_F69C7400-F52C-4BB9-96C9-195D2AB05E84']//div[text()='Add']";
    }
    public static String IPCC_Codes_Level_3_Add_Button() {
        return "//div[@id='control_F72D8B30-CE50-4BDC-829C-C86335F2B235']//div[text()='Add']";
    }
    public static String IPCC_Codes_Level_4_Add_Button() {
        return "//div[@id='control_29063BC4-9B8E-4DE5-BFBE-E0176A9C422E']//div[text()='Add']";
    }
    public static String IPCC_Codes_Level_2_Panel() {
        return "//div[text()='IPCC Codes Level 2']";
    }
    public static String IPCC_Codes_Level_3_Panel() {
        return "//div[text()='IPCC Codes Level 3']";
    }
    public static String IPCC_Codes_Level_4_Panel() {
        return "//div[text()='IPCC Codes Level 4']";
    }
    public static String IPCC_code() {
        return "(//div[@id='control_B2534729-5895-4BFE-8C55-81495EF7BD64']//input)[1]";
    }
    public static String IPCC_code2() {
        return "(//div[@id='control_792082BE-3254-4A26-9891-DB9CF2B888FE']//input)[1]";
    }
    public static String IPCC_code3() {
        return "(//div[@id='control_E91AE7E4-5F9D-48B6-A9B7-0D95400CD8CD']//input)[1]";
    }
    public static String IPCC_code4() {
        return "(//div[@id='control_7AA10AF1-DE90-4A94-A54B-B4A406029044']//input)[1]";
    }
    public static String Level2_CloseButton() {
        return "(//div[@id='form_333739F5-E675-4064-8C68-08748A796BAB']//i[@class='close icon cross'])[1]";
    }
    public static String Level3_CloseButton() {
        return "(//div[@id='form_58BBC78A-D268-4113-A23B-3F1B6EB5EE81']//i[@class='close icon cross'])[1]";
    }
    public static String Level4_CloseButton() {
        return "(//div[@id='form_9FD3BB6E-4998-4C99-96C0-303740FBA6C3']//i[@class='close icon cross'])[1]";
    }
    public static String Level2_process_flow() {
        return "//div[@id='btnProcessFlow_form_333739F5-E675-4064-8C68-08748A796BAB']";
    }
    public static String Level3_process_flow() {
        return "//div[@id='btnProcessFlow_form_58BBC78A-D268-4113-A23B-3F1B6EB5EE81']";
    }
    public static String Level4_process_flow() {
        return "//div[@id='btnProcessFlow_form_9FD3BB6E-4998-4C99-96C0-303740FBA6C3']";
    }
    public static String OperateMaintenance() {
        return "//label[text()='Operate Maintenance']//..//i";
    }
    public static String MonitoringMaintenance() {
        return "//label[text()='Monitoring Maintenance']//..//i";
    }
    public static String RTM_CloseButton() {
        return "(//div[@id='form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']//i[@class='close icon cross'])[1]";
    }
    public static String ActivityReference_Header() {
        return "//div[text()='Activity reference']";
    }
    public static String UnitOfMeasure_Dropdown() {
        return "//div[@id='control_D46A0228-6792-415B-9825-9F3536F222CB']";
    }
    public static String UnitOfMeasure_Dropdown2() {
        return "//div[@id='control_3A9F1A9C-8D17-497A-AA2C-9A32FF5FB91E']";
    }
    public static String UnitOfMeasure_Dropdown3() {
        return "//div[@id='control_14F76B4E-7FD6-48A2-9F2D-C6EA67AA73F0']";
    }
    public static String UnitOfMeasure_Dropdown4() {
        return "//div[@id='control_A187E79B-942C-44B8-811A-472BC5A550C6']";
    }
    public static String UnitOfMeasure_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String navigate_Carbon_Footprint() {
        return "//label[text()='Carbon Footprint']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static String RTM_Record(String data) {
        return "(//div[@id='control_5E46125E-0C20-451B-8019-9174EB0D51A7']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String Level2_Record(String data) {
        return "(//div[@id='control_F69C7400-F52C-4BB9-96C9-195D2AB05E84']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String Level3_Record(String data) {
        return "(//div[@id='control_F72D8B30-CE50-4BDC-829C-C86335F2B235']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String Level4_Record(String data) {
        return "(//div[@id='control_29063BC4-9B8E-4DE5-BFBE-E0176A9C422E']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String RTM_process_flow() {
        return "//div[@id='btnProcessFlow_form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']";
    }
    public static String Response_Team_Add_Button() {
        return "//div[@id='control_5E46125E-0C20-451B-8019-9174EB0D51A7']//div[text()='Add']";
    }
    public static String Role() {
        return "(//div[@id='control_32F8C182-5D50-4B2B-93C4-F1D20848D03D']//input)[1]";
    }
    public static String SaveButton() {
        return "//div[@id='btnSave_form_A5556A16-D6A7-4CAE-AD28-73E89E7600F2']";
    }
    public static String SaveButton_Level2() {
        return "//div[@id='btnSave_form_333739F5-E675-4064-8C68-08748A796BAB']";
    }
    public static String SaveButton_Level3() {
        return "//div[@id='btnSave_form_58BBC78A-D268-4113-A23B-3F1B6EB5EE81']";
    }
    public static String SaveButton_Level4() {
        return "//div[@id='btnSave_form_9FD3BB6E-4998-4C99-96C0-303740FBA6C3']";
    }
    public static String RTM_SaveButton() {
        return "//div[@id='btnSave_form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']";
    }
    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[text()='Save supporting documents']";
    }
    public static String TeamName() {
        return "(//div[@id='control_FED97497-13D2-4446-A649-FAF35715B218']//input)[1]";
    }
    public static String Team_Tab() {
        return "//div[text()='Team']";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_dropdown() {
        return "//div[@id='control_367581CE-32CB-4923-9B24-1FDF8DC5747C']";
    }
    public static String fullName_Dropdown() {
        return "//div[@id='control_6EE2438D-2B28-4A11-8E8E-B81E57A86B27']";
    }
    public static String impactType_Option(String data){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String impactType_Dropdown() {
        return "//div[@id='control_8DA377C4-295A-4FCC-B7B3-B2269151B655']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Add_Button() {
        return "//div[text()='Add']";
    }
    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_A5556A16-D6A7-4CAE-AD28-73E89E7600F2']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
}
