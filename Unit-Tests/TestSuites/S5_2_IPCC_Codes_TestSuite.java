/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_IPCC_Codes_TestSuite {

    static TestMarshall instance;

    public S5_2_IPCC_Codes_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //FR1-Capture IPCC Codes - Main Scenario
    @Test
    public void FR1_Capture_IPCC_Codes_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\IPCC Codes v5.2\\FR1-Capture IPCC Codes - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    
    //FR2-Capture IPCC Codes Level 2 - Main Scenario   
    @Test
    public void FR2_Capture_IPCC_Codes_Level_2_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\IPCC Codes v5.2\\FR2-Capture IPCC Codes Level 2 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-Capture IPCC Codes Level 3 - Main Scenario
    @Test
    public void FR3_Capture_IPCC_Codes_Level_3_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\IPCC Codes v5.2\\FR3-Capture IPCC Codes Level 3 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4-Capture IPCC Codes Level 4 - Main Scenario
    @Test
    public void FR4_Capture_IPCC_Codes_Level_4_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\IPCC Codes v5.2\\FR4-Capture IPCC Codes Level 4 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5-View Reports - Main Scenario
    @Test
    public void FR5_View_Reports_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\IPCC Codes v5.2\\FR5-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
